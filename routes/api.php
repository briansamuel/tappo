<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::namespace('Api')->group(function () {
    Route::prefix('v1')->group(function () {
        Route::post('auth/login', 'CustomerController@login');
        Route::post('auth/register', 'CustomerController@register');

        Route::group(['middleware' => 'api'], function () {

            Route::get('refresh', 'CustomerController@refresh');
            Route::get('me', 'CustomerController@me');
            Route::post('updateProfile', 'CustomerController@editAction');
            Route::post('changePassword', 'CustomerController@changePasswordAction');
            Route::post('changeBackground', 'CustomerController@changeBackgroundAction');
        });
    });
});
