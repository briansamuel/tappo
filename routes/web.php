<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::domain('senhos.local')->group(function () {


Route::domain(Config::get('domain.web.domain'))->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });
    Route::namespace('Frontsite')->group(function () {

        Route::get('/{slug}', ['uses' => 'CustomerController@slug'])->name('customerProfile');
    });
});

Route::domain(Config::get('domain.web.admin'))->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('denied-permission', function () {
            return view('admin.pages.permission_denied');
        });
        // login
        Route::get('/login', ['uses' => 'LoginController@login'])->name('login');
        Route::post('/login', ['uses' => 'LoginController@loginAction'])->name('loginAction');
        Route::get('/logout', ['uses' => 'LoginController@logout'])->name('logout');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('resetPassword');
        Route::get('/active-user', ['uses' => 'LoginController@activeUser'])->name('activeUser');
        Route::get('/active-agent', ['uses' => 'LoginController@activeAgent'])->name('activeAgent');
        Route::get('/active-guest', ['uses' => 'LoginController@activeGuest'])->name('activeGuest');
    });

    Route::middleware(['auth'])->group(function () {

        Route::get('/', ['uses' => 'Admin\DashboardController@index'])->name('dash-board');
        Route::get('/welcome', ['uses' => 'Admin\DashboardController@index'])->name('welcome');

        Route::namespace('Admin')->group(function () {
            // my profile
            Route::get('/my-profile', ['uses' => 'MyProfileController@profile'])->name('profile');
            Route::post('/my-profile', ['uses' => 'MyProfileController@updateProfile'])->name('profile.update');

            // change password
            Route::get('/change-password', ['uses' => 'MyProfileController@changePassword'])->name('profile.changePassword');
            Route::put('/change-password', ['uses' => 'MyProfileController@changePasswordAction'])->name('profile.changePasswordAction');

            Route::namespace('CMS')->group(function () {

                // Page
                Route::get('/page', ['uses' => 'PageController@index'])->name('page.list');
                Route::get('/page/add', ['uses' => 'PageController@add'])->name('page.add');
                Route::post('/page/add', ['uses' => 'PageController@add'])->name('page.add.action');
                Route::get('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit');
                Route::post('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit.action');
                Route::post('/page/edit', ['uses' => 'PageController@editManyAction'])->name('page.edit.many.action');
                Route::get('/page/delete/{service_id}', ['uses' => 'PageController@delete'])->name('page.delete');
                Route::post('/page/delete', ['uses' => 'PageController@deletemany'])->name('page.delete.many');
                Route::post('/page/save', ['uses' => 'PageController@save'])->name('page.save');
                Route::get('/page/ajax/get-list', ['uses' => 'PageController@ajaxGetList'])->name('page.ajax.getList');


                // User
                Route::get('/user', ['uses' => 'UsersController@index'])->name('user.list');
                Route::get('/user/add', ['uses' => 'UsersController@add'])->name('user.add');
                Route::post('/user/add', ['uses' => 'UsersController@addAction'])->name('user.add.action');
                Route::get('/user/detail/{user_id}', ['uses' => 'UsersController@detail'])->name('user.detail');
                Route::get('/user/edit/{user_id}', ['uses' => 'UsersController@edit'])->name('user.edit');
                Route::post('/user/edit', ['uses' => 'UsersController@editManyAction'])->name('user.edit.many.action');
                Route::post('/user/edit/{user_id}', ['uses' => 'UsersController@editAction'])->name('user.edit.action');
                Route::get('/user/delete', ['uses' => 'UsersController@deleteMany'])->name('user.delete.many');
                Route::get('/user/delete/{user_id}', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::post('/user/delete', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::get('/user/ajax/get-list', ['uses' => 'UsersController@ajaxGetList'])->name('user.ajax.getList');

                // Customer
                Route::get('/customer', ['uses' => 'CustomerController@index'])->name('customer.list');
                Route::get('/customer/add', ['uses' => 'CustomerController@add'])->name('customer.add');
                Route::post('/customer/add', ['uses' => 'CustomerController@addAction'])->name('customer.add.action');
                Route::get('/customer/detail/{user_id}', ['uses' => 'CustomerController@detail'])->name('customer.detail');
                Route::get('/customer/edit/{user_id}', ['uses' => 'CustomerController@edit'])->name('customer.edit');
                Route::post('/customer/edit', ['uses' => 'CustomerController@editManyAction'])->name('customer.edit.many.action');
                Route::post('/customer/edit/{user_id}', ['uses' => 'CustomerController@editAction'])->name('customer.edit.action');
                Route::get('/customer/delete', ['uses' => 'CustomerController@deleteMany'])->name('customer.delete.many');
                Route::get('/customer/delete/{user_id}', ['uses' => 'CustomerController@delete'])->name('customer.delete');
                Route::post('/customer/delete', ['uses' => 'CustomerController@delete'])->name('customer.delete');
                Route::get('/customer/ajax/get-list', ['uses' => 'CustomerController@ajaxGetList'])->name('customer.ajax.getList');

                // User Group
                Route::get('/user-group', ['uses' => 'UserGroupController@index'])->name('user_group.list');
                Route::get('/user-group/add', ['uses' => 'UserGroupController@add'])->name('user_group.add');
                Route::post('/user-group/add', ['uses' => 'UserGroupController@addAction'])->name('user_group.add.action');
                Route::get('/user-group/detail/{guest_id}', ['uses' => 'UserGroupController@detail'])->name('user_group.detail');
                Route::get('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@edit'])->name('user_group.edit');
                Route::post('/user-group/edit', ['uses' => 'UserGroupController@editManyAction'])->name('user_group.edit.many.action');
                Route::post('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@editAction'])->name('user_group.edit.action');
                Route::get('/user-group/delete', ['uses' => 'UserGroupController@deleteMany'])->name('user_group.delete.many');
                Route::get('/user-group/delete/{guest_id}', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::post('/user-group/delete', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::get('/user-group/ajax/get-list', ['uses' => 'UserGroupController@ajaxGetList'])->name('user_group.ajax.getList');

                // Logs User
                Route::get('/logs-user', ['uses' => 'LogsUserController@index'])->name('logs_user.list');
                Route::get('/logs-user/detail/{id}', ['uses' => 'LogsUserController@detail'])->name('logs_user.detail');
                Route::get('/logs-user/ajax/get-list', ['uses' => 'LogsUserController@ajaxGetList'])->name('logs_user.ajax.getList');

                // Menu
                Route::get('/menus', ['uses' => 'MenuController@index'])->name('menu.index');

                // Custom Css
                Route::get('/theme-option', ['uses' => 'ThemeOptionsController@option'])->name('theme_option.index');
                Route::post('/theme-option', ['uses' => 'ThemeOptionsController@optionAction'])->name('theme_option.action');

                // Custom Css
                Route::get('/custom-css', ['uses' => 'CustomCssController@index'])->name('custom_css.index');
                Route::post('/custom-css', ['uses' => 'CustomCssController@editAction'])->name('custom_css.editAction');

                // Contacts
                Route::get('/contact', ['uses' => 'ContactController@index'])->name('contact.index');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::get('/contact/edit/{id}', ['uses' => 'ContactController@edit'])->name('contact.edit');
                Route::post('/contact/edit', ['uses' => 'ContactController@editManyAction'])->name('contact.edit.many.action');
                Route::post('/contact/edit/{id}', ['uses' => 'ContactController@editAction'])->name('contact.edit.action');
                Route::get('/contact/delete', ['uses' => 'ContactController@deleteMany'])->name('contact.delete.many');
                Route::get('/contact/delete/{id}', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::post('/contact/delete', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::post('/contact/{id}/reply', ['uses' => 'ContactController@replyAction'])->name('contact.reply.action');

                // Setting
                Route::get('/settings-general', ['uses' => 'SettingController@general'])->name('setting.general');
                Route::post('/settings-general', ['uses' => 'SettingController@generalAction'])->name('setting.general.action');
                Route::post('/settings-login-social', ['uses' => 'SettingController@loginSocialAction'])->name('setting.login_social.action');
                Route::get('/settings-email', ['uses' => 'SettingController@email'])->name('setting.email');
                Route::get('/settings-social-login', ['uses' => 'SettingController@loginSocial'])->name('setting.login_social');
                Route::get('/settings-notification', ['uses' => 'SettingController@notification'])->name('setting.login_social');
                Route::post('/settings-notification', ['uses' => 'SettingController@notificationAction'])->name('setting.login_social');

                // Gallery
                Route::get('/gallery', ['uses' => 'GalleryController@index'])->name('gallery.index');
            });
        });
        Route::namespace('General')->group(function () {
            Route::post('/upload-image', ['uses' => 'UpLoadImageController@uploadImage'])->name('uploadImage');

            //Route::post('/filemanager/upload.php', ['uses' => 'UpLoadImageController@uploadImage'])->name('fileManagerUpload');
        });
    });
});
