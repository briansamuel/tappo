<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use App\Services\CustomerService;
use Illuminate\Http\Request;
use App\Services\ValidationService;
use App\Helpers\ArrayHelper;

class CustomerController extends Controller
{

    protected $request;
    protected $validator;

    public function __construct(Request $request, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
    }

    public function slug($slug)
    {

        $user = CustomerService::getUserInfoByUserName($slug);
        if (!$user) {
            $user = CustomerService::getUserInfoByQrcode($slug);
            if (!$user) {
                abort('404');
            }
            
        }
        $listBackground = ArrayHelper::listBackground();
        $background_index = $user->background ?? 0;

        $background =  $listBackground[$background_index];
        // Edit profile Card
        return view('frontsite.profiles.index', compact('user', 'background'));
    }
}
