<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Helpers\UploadImage;
use App\Http\Controllers\Controller;
use App\Services\LogsUserService;
use App\Services\UserGroupService;
use App\Services\ValidationService;
use App\Services\CustomerService;
use Illuminate\Http\Request;
use Session;

class CustomerController extends Controller
{
    protected $request;
    protected $customerService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, CustomerService $customerService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->customerService = $customerService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.customers.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->customerService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {
        $listGroup = UserGroupService::getGroupActive();

        return view('admin.customers.add', ['listGroup' => $listGroup]);
    }

    public function addAction()
    {
        $params = $this->request->only('email', 'username', 'full_name', 'password', 'group_id', 'profile_avatar');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_user_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        $upload = UploadImage::uploadAvatar($params['profile_avatar'], 'user');
        if (!$upload['success']) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        $params['url_avatar'] = $upload['url'];

        $add = CustomerService::add($params);
        if ($add) {
            //add log
            $log['action'] = "Thêm mới 1 User có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $this->CustomerService->sendMailActiveUser($params['email']);
            $data['success'] = true;
            $data['message'] = "Thêm mới User thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới User !";
        }

        return response()->json($data);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->CustomerService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " User thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " User thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " User thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $user = $this->CustomerService->detail($id);
        $delete = $this->CustomerService->delete($id);
        if ($delete) {
            //add log
            $log['action'] = "Xóa User thành công có ID = " . $id;
            $log['content'] = json_encode($user);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa user thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa user không thành công', 'danger');
        }

        return redirect("customers");
    }

    public function detail($id)
    {
        $userInfo = $this->customerService->detail($id);
        $listGroup = UserGroupService::getGroupActive();

        return view('admin.customers.detail', ['userInfo' => $userInfo, 'listGroup' => $listGroup]);
    }

    public function edit($id)
    {
        $userInfo = $this->customerService->detail($id);
        $listGroup = UserGroupService::getGroupActive();
        $listIconSocial = ArrayHelper::listSocial();
        return view('admin.customers.edit', ['userInfo' => $userInfo, 'listIconSocial' => $listIconSocial]);
    }

    public function editAction($id)
    {
        $params = $this->request->only(['full_name', 'password', 'list_social', 'avatar', 'status']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_user_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        if (isset($params['avatar'])) {
            $upload = UploadImage::uploadAvatar($params['avatar'], 'user');
            if (!$upload['success']) {
                return response()->json(Message::get(13, $lang = '', []), 400);
            }
            $params['avatar'] = $upload['url'];
        }

        if (isset($params['password']) && $params['password'] != '') {
            $params['password'] = bcrypt($params['password']);
        }

        if (isset($params['list_social']) && $params['list_social'] != '') {

            if (is_array($params['list_social'])) {
                $start = count($params['list_social']) - 1;
                for ($i = $start; $i >= 0; $i--) {
                    if (empty($params['list_social'][$i]['label']) || empty($params['list_social'][$i]['content'])) {
                        unset($params['list_social'][$i]);
                    }
                }
            }
            $params['list_social'] = json_encode($params['list_social']);
        }
        $edit = $this->customerService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập User thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập User thành công !!!";
        return response()->json($data);
    }

    public function editManyAction()
    {
        $params = $this->request->only(['status', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->CustomerService->updateMany($params['ids'], ['status' => $params['status']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " User thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập nhiều User thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " User thành công !!!";
        return response()->json($data);
    }
}
