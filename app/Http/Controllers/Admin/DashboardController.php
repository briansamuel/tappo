<?php

namespace App\Http\Controllers\Admin;

use App\Services\DashboardService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DashboardController extends Controller
{
    

    public function __construct()
    {
        
        
    }

    /** 
    * ======================
    * Method:: INDEX
    * ======================
    */

    public function index()
    {
        // take 5 new user
       

        return view('admin.dash-board');
    }
}
