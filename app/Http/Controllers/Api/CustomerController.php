<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CustomerService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Services\ValidationService;
use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Helpers\UploadImage;
use Exception;

class CustomerController extends Controller
{

    protected $request;
    protected $validator;
    protected $customerService;
    /**
     * Create a new CustomerController instance.
     *
     * @return void
     */
    public function __construct(Request $request, ValidationService $validator, CustomerService $customerService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->customerService = $customerService;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        $token = auth('api')->attempt($credentials);
        if (!$token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    public function editAction()
    {
        try {
            $params = $this->request->only(['full_name', 'description', 'list_social', 'address', 'avatar']);
            $user = auth('api')->user();
            // $validator = $this->validator->make($params, 'edit_customer_fields');
            // if ($validator->fails()) {
            //     return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
            // }
            foreach ($params as $key => $item) {
                if (!$item || $item == '') {
                    unset($params[$key]);
                }
            }
            if (isset($params['avatar'])) {
                $upload = UploadImage::uploadAvatar($params['avatar'], 'user');
                if (!$upload['success']) {
                    return response()->json(Message::get(13, $lang = '', []), 400);
                }
                $params['avatar'] = $upload['url'];
            }

            $edit = $this->customerService->edit($user->id, $params);
            if (!$edit) {
                return response()->json(Message::get(13, $lang = '', []), 400);
            }

            //add log

            $data['status'] = true;
            $data['message'] = "Cập nhập User thành công !!!";
            $data['data'] = $params;
        } catch (Exception $e) {
            $data['status'] = false;
            $data['message'] = $e->getMessage();
        }

        return response()->json($data);
    }

    public function register(Request $request)
    {
        try {
            $params = $request->only('email', 'username', 'password', 'confirm_password', 'full_name');
            $validator = $this->validator->make($params, 'add_customer_fields');

            if ($validator->fails()) {
                $data['status'] = false;
                $arrayMessage = Message::get(1, $lang = '', $validator->errors()->all());
                $data['message'] = $arrayMessage['error']['errors'][0];

                return response()->json($data);
            }
            $add = $this->customerService->add($params);
            if (!$add) {
                $data['status'] = false;
                $arrayMessage = Message::get(14, $lang = '', []);
                $data['message'] = $arrayMessage['error']['message'];
                return response()->json($data, 200);
            }

            $credentials = $request->only('email', 'password');
            //add log
            $token = auth('api')->attempt($credentials);
            if (!$token) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            return $this->respondWithToken($token);
        } catch (Exception $e) {
            $data['status'] = false;
            $data['message'] = $e->getMessage();
        }

        return response()->json($data);
    }

    public function changeBackgroundAction()
    {
        try {
            $params = $this->request->only(['background']);
            $user = auth('api')->user();

            $edit = $this->customerService->edit($user->id, $params);
            if (!$edit) {
                return response()->json(Message::get(13, $lang = '', []), 400);
            }

            //add log

            $data['status'] = true;
            $data['message'] = "Cập nhập ảnh nền thành công";
            $data['data'] = $params;
        } catch (Exception $e) {
            $data['status'] = false;
            $data['message'] = $e->getMessage();
        }

        return response()->json($data);
    }

    public function changePasswordAction(Request $request)
    {
        $params = $request->only('password', 'new_password', 'confirm_new_password');
        $validator = $this->validator->make($params, 'change_password_fields');
        if ($validator->fails()) {
            $data['status'] = false;
            $data['message'] = Message::get(1, $lang = '', $validator->errors()->all());

            return response()->json($data);
        }

        try {
            $checkPass = CustomerService::checkCurrentPassword($params['password']);
            if (!$checkPass) {
                $data['status'] = false;
                $data['message'] = 'Sai mật khẩu hiện tại!';
                return response()->json($data);
            }
            $result = CustomerService::updatePassword($params['new_password']);
            if ($request) {
                $data['status'] = true;
                $data['message'] = "Đổi mật khẩu thành công!";
                $data['data'] = $params;
            }
        } catch (Exception $e) {
            $data['status'] = false;
            $data['message'] = $e->getMessage();
        }


        return response()->json($data);
    }
}
