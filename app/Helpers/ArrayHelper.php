<?php

namespace App\Helpers;

class ArrayHelper
{
    /*
     *
     */

    public static function removeArrayNull($params)
    {
        if (!is_array($params)) {
            return $params;
        }

        if (isset($params['_token'])) {
            unset($params['_token']);
        }

        return collect($params)
            ->reject(function ($item) {
                return is_null($item);
            })
            ->flatMap(function ($item, $key) {

                return is_numeric($key)
                    ? [self::removeArrayNull($item)]
                    : [$key => self::removeArrayNull($item)];
            })
            ->toArray();
    }

    public static function removeToken($params)
    {
        if (!is_array($params)) {
            return $params;
        }

        if (isset($params['_token'])) {
            unset($params['_token']);
        }

        return $params;
    }

    public static function listSocial()
    {
        return [
            'facebook',
            'instagram',
            'messenger',
            'twitter',
            'tumblr',
            'linkedin',
            'skype',
            'youtube',
            'pinterest',
            'tiktok',
            'email',
            'website',
            'sms',
            'phone',

        ];
    }

    public static function listBackground()
    {
        return [
            ['#6944ff', '#ff2846'],
            ['#ef9a9a'],
            ['#ef5350'],
            ['#c62828'],
            ['#a5d6a7'],
            ['#66bb6a'],
            ['#2e7d32'],
            ['#90caf9'],
            ['#f2a5f5'],
            ['#1565c0'],
            ['#fff59d'],
            ['#ffee58'],
            ['#f9a825'],
            ['#fffe082'],
            ['#ffca28'],
            ['#ffa000'],
            ['#f48fb1'],
            ['#ec407a'],
            ['#ad1457'],
            ['#ce93d8'],
            ['#ab47bc'],
            ['#6a1b9a'],
            ['#f48fb1'],
            ['#ffffff'],
            ['#000000'],
            ['#bdbdbd'],
            ['#424242'],
            ['#ffeb3b', '#f44336', '#3f51b5', '#009688'],
            ['#e91e63', '#f44336'],

        ];
    }
}
