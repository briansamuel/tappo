<?php

namespace App\Services;


use Validator;

class ValidationService
{

    /*
    * function make validation
    */
    public function make($params, $type)
    {
        $validator = Validator::make(
            $params,
            $this->getRules($type),
            $this->getCustomMessages()
        );
        return $validator;
    }

    /*
    * function get rule config
    */
    public function getRules($type)
    {
        $rules = [
            'update_my_profile_fields' => [
                'full_name' => self::getRule('require_field'),
            ],
            'change_password_fields' => [
                'password' => self::getRule('password'),
                'new_password' => self::getRule('password'),
                'confirm_new_password' => self::getRule('password'),
            ],
            'add_customer_fields' => [
                'email' => self::getRule('customer_email'),
                'username' => self::getRule('customer_username'),
                'full_name' => self::getRule('full_name'),
                'password' => self::getRule('password'),
                'confirm_password' => self::getRule('password'),
            ],
            'add_user_fields' => [
                'email' => self::getRule('email'),
                'username' => self::getRule('username'),
                'full_name' => self::getRule('full_name'),
                'password' => self::getRule('password')
            ],
            'edit_user_fields' => [
                'full_name' => self::getRule('full_name'),
                'status' => self::getRule('require_field')
            ],

            'edit_page_fields' => [
                'page_title' => self::getRule('require_field'),
                'page_description' => self::getRule('require_field'),
                'page_content' => self::getRule('require_field'),

            ],
            'add_news_fields' => [
                'post_title' => self::getRule('require_field'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),

            ],
            'edit_news_fields' => [
                'post_title' => self::getRule('require_field'),
                'post_description' => self::getRule('require_field'),
                'post_content' => self::getRule('require_field'),

            ],

            'edit_contact_fields' => [
                'status' => self::getRule('require_field')
            ],
            'add_contact_reply_fields' => [
                'contact_id' => self::getRule('require_field'),
                'message' => self::getRule('require_field')
            ],
            'edit_comment_fields' => [
                'comment_status' => self::getRule('require_field')
            ],
            'edit_review_fields' => [
                'review_status' => self::getRule('require_field')
            ],

        ];

        return isset($rules[$type]) ? $rules[$type] : array();
    }


    /*
    * function get rule
    */
    public function getRule($rule)
    {
        $rules = [
            'limit' => 'numeric',
            'offset' => 'numeric',
            'user_id' => 'numeric|required',
            'username' => 'required',
            'full_name' => 'required',
            'phone_number' => 'digits_between:10,15|required',
            'password' => 'nullable|min:5|required',
            'require_field' => 'required',
            'email' => 'email',
            'payment_method' => 'required',
            'ip' => 'ip',
            'amount' => 'numeric|required',
            'number' => 'numeric',
            'host_name' => 'required|unique:hosts',
            'customer_username' => 'required|unique:customers',
            'customer_email' => 'required|unique:customers,email',
        ];

        return $rules[$rule];
    }

    /**
     * function get custom messages
     */
    public function getCustomMessages()
    {
        $messages = [
            'required' => 'Không bỏ trống trường :attribute',
            'unique' => ':attribute đã tồn tại, vui lòng thay đổi nội dung',
        ];

        return $messages;
    }
}
