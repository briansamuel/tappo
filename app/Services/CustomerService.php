<?php

namespace App\Services;

use App\Models\CustomerModel;
use App\Notifications\ActiveUserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class CustomerService
{

    public static function totalRows()
    {
        $result = CustomerModel::totalRows();
        return $result;
    }

    public static function add($params)
    {
        $insert['username'] = $params['username'];
        $insert['password'] = bcrypt($params['password']);
        $insert['email'] = $params['email'];
        $insert['full_name'] = $params['full_name'];
        $insert['qr_code'] = Str::random(6);
        $insert['list_social'] = '[]';
        $insert['status'] = 'active';
        $insert['background'] = 0;
        $insert['created_at'] = date("Y-m-d H:i:s");
        $insert['updated_at'] = date("Y-m-d H:i:s");
        return CustomerModel::insert($insert);
    }

    public function edit($id, $params)
    {

        $params['updated_at'] = date("Y-m-d H:i:s");
        return CustomerModel::updateUser($id, $params);
    }

    public function deleteMany($ids)
    {
        return CustomerModel::deleteManyUser($ids);
    }

    public function updateMany($ids, $data)
    {
        return CustomerModel::updateManyUser($ids, $data);
    }

    public function delete($ids)
    {
        return CustomerModel::deleteUser($ids);
    }

    public function detail($id)
    {
        return CustomerModel::findById($id);
    }

    public static function getUserInfoByUserName(string $user_name)
    {
        return CustomerModel::findByKey('username', $user_name);
    }

    public static function getUserInfoByQrcode(string $qr_code)
    {
        return CustomerModel::findByKey('qr_code', $qr_code);
    }

    public static function getUserInfoByEmail(string $email)
    {
        return CustomerModel::findByKey('email', $email);
    }


    public static function updatePasswordByEmail(string $email, string $password)
    {
        return CustomerModel::where('email', $email)->update([
            'password' => bcrypt($password),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }

    public static function updateProfile(array $params)
    {
        $user = Auth::guard('admin')->user();

        $user->full_name = $params['full_name'];
        $user->avatar = isset($params['avatar']) ? $params['avatar'] : $user->avatar;

        return $user->save();
    }

    public static function checkCurrentPassword(string $password)
    {
        $user = Auth::guard('api')->user();
        if (Hash::check($password, $user->password)) {
            return true;
        }
        return false;
    }

    public static function updatePassword(string $password)
    {
        $user = Auth::guard('api')->user();

        $user->password = bcrypt($password);

        return $user->save();
    }

    public function getList(array $params)
    {
        $total = self::totalRows();
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = CustomerModel::getMany($pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public function getListIDs($data)
    {

        $ids = array();

        foreach ($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

    public function sendMailActiveUser($email)
    {
        $user = UserService::getUserInfoByEmail($email);

        $user->notify(new ActiveUserRequest($user->active_code));

        return response()->json([
            'success' => true,
            'message' => 'Chúng tôi đã gửi email kích hoạt tài khoản đến email ' . $email . '!'
        ]);
    }

    public function activeUser($id)
    {
        $data['status'] = 'active';
        $data['email_verified_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        return CustomerModel::updateUser($id, $data);
    }
}
