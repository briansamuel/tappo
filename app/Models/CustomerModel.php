<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CustomerModel extends Authenticatable implements JWTSubject
{
    protected $table = 'customers';
    protected $guard = 'customer';

    use Notifiable;


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }



    public static function getAll()
    {
        $result = CustomerModel::get();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = CustomerModel::skip($offset)->take($pagination['perpage']);
        if (isset($filter['status']) && $filter['status'] != "") {
            $query->where('status', '=', $filter['status']);
        }
        if (isset($filter['email']) && $filter['email'] != "") {
            $query->where('email', 'like', "%" . $filter['email'] . "%");
        }
        if (isset($filter['username']) && $filter['username'] != "") {
            $query->where('username', 'like', "%" . $filter['username'] . "%");
        }
        if (isset($filter['is_root']) && $filter['is_root'] != "") {
            $query->where('is_root', '=', $filter['is_root']);
        }
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if (isset($sort['field']) && $sort['field'] === "created_at") {
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows()
    {
        $result = CustomerModel::count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = CustomerModel::select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = CustomerModel::select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function updateUser($id, $data)
    {
        return CustomerModel::where('id', $id)->update($data);
    }

    public static function deleteManyUser($ids)
    {
        return CustomerModel::whereIn('id', $ids)->delete();
    }

    public static function updateManyUser($ids, $data)
    {
        return CustomerModel::whereIn('id', $ids)->update($data);
    }

    public static function deleteUser($id)
    {
        return CustomerModel::where('id', $id)->delete();
    }

    public static function takeNewUser($quantity)
    {
        return CustomerModel::orderBy('id', 'DESC')->limit($quantity)->get();
    }
}
