<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class LogsUserModel
{

    protected static $table = 'logs_user';

    public static function getAll()
    {
        $result = DB::table(self::$table)->get();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);
        if (isset($filter['email']) && $filter['email'] != "") {
            $query->where('email', 'like', "%" . $filter['email'] . "%");
        }
        if (isset($filter['username']) && $filter['username'] != "") {
            $query->where('username', 'like', "%" . $filter['username'] . "%");
        }
        if (isset($filter['full_name']) && $filter['full_name'] != "") {
            $query->where('full_name', 'like', "%" . $filter['full_name'] . "%");
        }
        if (isset($filter['timestamp']) && $filter['timestamp'] != "") {
            $time_filter = explode(" - ", $filter['timestamp']);
            $start_time = strtotime(date("Y-m-d 00:00:00", strtotime($time_filter[0])));
            $end_time = strtotime(date("Y-m-d 23:59:59", strtotime($time_filter[1])));

            $query->where('timestamp', '>=', $start_time);
            $query->where('timestamp', '<', $end_time);
        }
        if (isset($sort['field']) && $sort['field'] === "timestamp") {
            $query->orderBy('timestamp', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows()
    {
        $result = DB::table(self::$table)->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function insert($data)
    {
        return DB::table(self::$table)->insert($data);
    }
}
