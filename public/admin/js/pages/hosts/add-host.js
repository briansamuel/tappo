"use strict";
var KTPortletTools = {
    init: function() {
        var e;
        toastr.options.showDuration = 1e3, (e = new KTPortlet("kt_portlet_tools_policy")),
            function() {
                var e = new KTPortlet("kt_portlet_tools_convenient");

            }(),
            function() {
                var e = new KTPortlet("kt_portlet_tools_gallery");

            }(),
            function() {
                var e = new KTPortlet("kt_portlet_tools_4");

            }(),
            function() {
                var e = new KTPortlet("kt_portlet_tools_5");

            }(),
            function() {
                var e = new KTPortlet("kt_portlet_tools_6");

            }()
    }
};
var objmap
jQuery(document).ready(function() {
    KTPortletTools.init();
    var marker;
    objmap = new GMaps({
        el: "#kt_gmap_2",
        zoom: 16,
        lat: 10.843295,
        lng: 106.762884,
        dragend: function(t) {
            //console.log(t);
        }
    });

    objmap.addListener('click', function(e) {
        //console.log(e);
        placeMarker(e);
    });
    
    function placeMarker(t) {

        objmap.removeMarkers();
        objmap.addMarker({
            position: t.latLng,
            title: "Vị trí",
            infoWindow: {
                content: '<span style="color:#000">Vị trí khách sạn của bạn</span>'
            }
        });
        $('#host_location').val(t.latLng.lat() + ',' + t.latLng.lng());
    }

    $('#kt_portlet_tools_convenient .kt-checkbox').each(function(index, element) {
        //let val_input = $(this).text();
        //console.log($(this).text());
        var list = index + ' => "' + $(this).text() + '",';
        $(this).children('input').val($(this).text());
        //$(this).val($(this).text()); 
        //console.log(list);
    });


});

var KTSelect2 = {
    init: function() {
        $("#kt_select_province, #kt_select2_1_validate").select2({
                placeholder: "Chọn tỉnh thành"
            }),
            $("#kt_select_district, #kt_select2_2_validate").select2({
                placeholder: "Chọn quận huyện",

            }),
            $("#kt_select_ward").select2({
                placeholder: "Chọn phường xã",

            }),
            $("#kt_select_host").select2({
                placeholder: "Chọn Khách sạn, Resort ...",

            }),
            $('#kt_select_province').on('select2:select', function(e) {

                var data = e.params.data;
                //console.log(data);// Do something
                console.log(data);
                $('#province_name').val(data.text);
                
                var districtSelect = $('#kt_select_district');
                districtSelect.select2('data', null);
                $.ajax({
                    type: 'GET',
                    url: '/host/ajax/get-district?_province_id=' + data.id,
                }).then(function(data) {

                   
                    districtSelect.html('');
                    var option = new Option('Chọn quận/huyện', 0, true, true);
                    districtSelect.append(option);
                    $('#district_name').val('');
                    data.forEach(function(i, index) {
                        var option = new Option(i._name, i.id, false, false);
                        districtSelect.append(option);
                    });
                    // create the option and append to Select2


                    // manually trigger the `select2:select` event

                    // districtSelect.trigger({
                    //     type: 'select2:select',
                    //     params: {
                    //         data: data
                    //     }
                    // });

                    
                });
            }),
            $('#kt_select_district').on('select2:select', function(e) {

                var data = e.params.data;
                //console.log(data);// Do something
                $('#district_name').val(data.text);
                GMaps.geocode({
                    address: data.text+', '+$('#province_name'),
                    callback: function(results, status) {
                        console.log('TEST');
                      if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        objmap.setCenter(latlng.lat(), latlng.lng());
                        objmap.setZoom(14);
                      }
                    }
                });
                var wardSelect = $('#kt_select_ward');
                wardSelect.select2('data', null);
                $.ajax({
                    type: 'GET',
                    url: '/host/ajax/get-ward?_district_id=' + data.id,
                }).then(function(data) {
                    
                    wardSelect.html('');
                    var option = new Option('Chọn phường/xã', '', true, true);
                    wardSelect.append(option);
                    $('#ward_name').val('');
                    data.forEach(function(i, index) {
                        var option = new Option(i._name, i.id, false, false);
                        wardSelect.append(option);
                    });
                    // create the option and append to Select2


                    // manually trigger the `select2:select` event
                    // wardSelect.trigger({
                    //     type: 'select2:select',
                    //     params: {
                    //         data: data
                    //     }
                    // });
                    
                });
            }),
            $('#kt_select_ward').on('select2:select', function(e) {

                var data = e.params.data;
                //console.log(data);// Do something
                $('#ward_name').val(data.text);
                
                

            })
    }
}

;
jQuery(document).ready(function() {
        KTSelect2.init()
    }

);
