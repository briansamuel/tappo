var list_chapter = [];
var list_story = [];
var current_chapter = 0;
var current_story = 0;
! function(t) {
    var e = {};

    function a(n) {
        if (e[n])
            return e[n].exports;
        var o = e[n] = {
            i: n,
            l: !1,
            exports: {}
        };
        return t[n].call(o.exports, o, o.exports, a),
            o.l = !0,
            o.exports
    }
    a.m = t,
        a.c = e,
        a.d = function(t, e, n) {
            a.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: n
            })
        },
        a.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }),
                Object.defineProperty(t, "__esModule", {
                    value: !0
                })
        },
        a.t = function(t, e) {
            if (1 & e && (t = a(t)),
                8 & e)
                return t;
            if (4 & e && "object" == typeof t && t && t.__esModule)
                return t;
            var n = Object.create(null);
            if (a.r(n),
                Object.defineProperty(n, "default", {
                    enumerable: !0,
                    value: t
                }),
                2 & e && "string" != typeof t)
                for (var o in t)
                    a.d(n, o, function(e) {
                            return t[e]
                        }
                        .bind(null, o));
            return n
        },
        a.n = function(t) {
            var e = t && t.__esModule ? function() {
                    return t.default
                } :
                function() {
                    return t
                };
            return a.d(e, "a", e),
                e
        },
        a.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        },
        a.p = "",
        a(a.s = 703)
}({
    703: function(t, e, a) {
        "use strict";
        var n, o = (n = {
            data: {
                type: "remote",
               
                source: {
                    read: {
                        url: "story/ajax/get-list",
                        method: 'get',
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e
                        }
                    }
                },
                pageSize: 20,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                // scroll: !0,
                // height: 350,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            columns: [{
                field: "id",
                title: "#",
                sortable: !1,
                width: 20,
                selector: {
                    class: "kt-checkbox--solid"
                },
                textAlign: "center"
            }, {
                field: "story_title",
                title: "TÊN TRUYỆN",
                width: 360,
                template: function(t) {
                    return "<strong>" + t.story_title + "</strong>"
                }
            }, { 
                field: "total_chapter",
                title: "SỐ CHƯƠNG",
                template: function(t) {
                    return "<strong>" + t.total_chapter + "</strong>"
                }
            }, {
                field: "story_author",
                title: "TÁC GIẢ",
                template: function(t) {
                    return "<strong>" + t.story_author + "</strong>"
                }
            }, {
                field: "story_status",
                title: "TÌNH TRẠNG  ",
                width: 100,
                template: function(t) {
                    var e = {
                        
                        'pending': {
                            title: "Đang chờ duyệt",
                            class: " kt-badge--danger"
                        },
                        'coming': {
                            title: "Đang ra",
                            class: " kt-badge--success"
                        },
                        'full': {
                            title: "Đã hoàn thành",
                            class: " kt-badge--info"
                        },
                        
                    };
                    return '<span class="kt-badge ' + e[t.story_status].class + ' kt-badge--inline kt-badge--pill">' + e[t.story_status].title + "</span>"
                }
            }, {
                field: "created_at",
                title: "NGÀY TẠO"
            }, {
                field: "Actions",
                title: "THAO TÁC",
                sortable: !1,
                width: 110,
                overflow: "visible",
                textAlign: "left",
                autoHide: !1,
                template: function(t) {
                    return '    <button type="button"  class="btn btn-sm btn-clean btn-icon btn-icon-sm kt_datatable_crawl" onclick="crawl_data()" title="Xem truyện" >                        <i class="flaticon-eye"></i>                    </button>                 <a href="story/edit/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Sửa đầu truyện">                        <i class="flaticon2-pen"></i>                    </a>                    <a href="story/delete/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Xóa đầu truyện" onclick="return confirm(\'Bạn có chắc muốn xóa đầu truyện này đi không ???\');">                        <i class="flaticon2-delete"></i>                    </a>                '
                }
            }]
        }, {

            init: function() {
                ! function() {
                    n.search = {
                        input: $("#story_title")
                    };
                }(),
                function() {
                    n.extensions = {
                            checkbox: {}
                        },
                        n.search = {
                            input: $("#story_title")
                        };
                    var t = $("#server_news_selection").KTDatatable(n);
                        $("#kt_datatable_delete_all").on("click", (function() {
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            $.ajax({
                                url: "news/delete",
                                method: 'post',
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    total: total_checked_id
                                },
                                success: function(){
                                    window.location.reload();
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        $(".kt_datatable_update_status_all").on("click", (function() {
                            var status = $(this).attr('attr');
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            $.ajax({
                                url: "news/edit",
                                method: 'POST',
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    total: total_checked_id,
                                    status: status
                                },
                                success: function(){
                                    window.location.reload();
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        $("#btn-crawl-chapter").on("click", (function() {
                            
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            list_chapter = [];
                            list_story = [];
                            current_chapter = 0;
                            current_story = 0;
                            $('#btn-crawl-story').prop('disabled', true);
                            $('#btn-crawl-story').addClass('disabled');
                            $('#btn-stop-crawl').prop('disabled', false);
                            $('#btn-stop-crawl').removeClass('disabled');
                            $('#btn-clear-console').prop('disabled', false);
                            $('#btn-clear-console').removeClass('disabled');
                            $('.result-message').html('<h4 class="kt-font-primary kt-font-bold">Đang lấy danh sách chương ...</h4>');
                            $.ajax({
                                url: "crawl/ajax/list-chapter",
                                method: 'POST',
                                timeout: 360000,
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    
                                },
                                success: function(data){
                                    var data = JSON.parse(data);
                                    console.log(data.data[0]);
                                    list_story = data.data;
                                    
                                    if(data.status == 'success') {
                                       
                                        
                                        o.callbackAddChapter();
                                    }
                                    $('.result-message').html(data.message);
                                    setTimeout(function () {
                                        
                                    }, 2500);
                                   
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        $(".kt_datatable_crawl").on("click", (function() {
                            console.log('TEST');
                        })),
                        $("#kt_form_status1").on("change", (function() {
                            t.search($(this).val().toLowerCase(), "status")
                        })),
                        $("#kt_form_type1").on("change", (function() {
                            t.search($(this).val().toLowerCase(), "Type")
                        })),
                        $("#kt_form_status1,#kt_form_type1").selectpicker(),
                        t.on("kt-datatable--on-click-checkbox kt-datatable--on-layout-updated", (function(e) {
                            var a = t.checkbox().getSelectedId().length;
                            $("#kt_datatable_selected_number1").html(a),
                                a > 0 ? $("#kt_datatable_group_action_form1").collapse("show") : $("#kt_datatable_group_action_form1").collapse("hide")
                        })),
                        $("#kt_modal_fetch_id_server").on("show.bs.modal", (function(e) {
                            for (var a = t.checkbox().getSelectedId(), n = document.createDocumentFragment(), o = 0; o < a.length; o++) {
                                var r = document.createElement("li");
                                r.setAttribute("data-id", a[o]),
                                    r.innerHTML = "Selected record ID: " + a[o],
                                    n.appendChild(r)
                            }
                            $(e.target).find(".kt-datatable_selected_ids").append(n)
                        })).on("hide.bs.modal", (function(t) {
                            $(t.target).find(".kt-datatable_selected_ids").empty()
                        }))
                }()
            },
            callbackAddChapter: function() {
                console.log('Callback');
                var story = list_story[current_story];
                list_chapter = story.list_chapter;
                $('.current-page').html(story.story_title+' - Số chương '+ list_chapter.length);
                console.log('Callback');
                if(list_chapter.length == 0) {
                    current_story = current_story+1;
                    
                    if(current_story < list_story.length) {
                        o.callbackAddChapter();
                    }

                    if(current_story == list_story.length && current_story.length != 0) {
                        o.updateCrawlLogs(list_story[current_story-1].story_id);
                        console.log('Đã hoàn thành');
                        setTimeout(function () {
                            $('.current_page').text('');
                            $('.result-message').html('<h4 class="kt-font-primary kt-font-bold"> ======= > Đã hoàn thành quá trình lấy dữ liệu <======= </h4>');
                            $('#list_story').append('<h3 class="kt-font-primary kt-font-bold"> ======= > Đã hoàn thành quá trình lấy dữ liệu <======= </h3>');
                            $('#btn-crawl-story').prop('disabled', false);
                            $('#btn-crawl-story').removeClass('disabled');
                        }, 3000);
                    } 
                } else {
                    o.addChapter(story.story_id, list_chapter[0]);
                }
            },
            updateCrawlLogs: function(id) {
                $.ajax({
                    url: "crawl/ajax/update-crawl-logs",
                    method: 'get',
                    type: 'json',
                    timeout: 30000,
                    data: {
                        _token: $("input[name='_token']").val(),
                        id: id,
                        
                    },
                    success: function(data){
                        
                    },
                    error: function(errors) {
                        console.log(errors);
                    },
                });
            },
            addChapter: function(id, url) {
                
                if(url != '' && url != null) {
                    $('.result-message').html('<span class="kt-font-primary kt-font-bold">Đang kiểm tra chương '+url+'</span>');
                    $.ajax({
                        url: "crawl/ajax/add-chapter",
                        method: 'post',
                        type: 'json',
                        timeout: 30000,
                        data: {
                            _token: $("input[name='_token']").val(),
                            id: id,
                            url: url
                        },
                        success: function(data){
                            var data = JSON.parse(data);
                            if(data.status == 'success') {
                                
                                
                            }
                            $('.result-message').html(data.message);
                            
                            current_chapter = current_chapter + 1;
                            
                            if(current_chapter < list_chapter.length) {
                                
                                $('#list_story').append('<div class="kt-section__desc"><span class="kt-font-danger kt-font-italic">'+url+'</span> : '+data.message+' </div>').slideDown("slow");
                                
                                o.addChapter(id, list_chapter[current_chapter]);
                            }
                            
                            if(current_chapter == list_chapter.length && list_chapter.length != 0) {

                                current_story = current_story + 1;
                                if(current_story < list_story.length) {
                                    console.log('Chuyển sang story tiếp theo');
                                    
                                    
                                    var story_id = list_story[current_story].story_id;
                                    list_chapter = list_story[current_story].list_chapter;
                                    current_chapter = 0;
                                    o.addChapter(story_id,list_chapter[0]);
                                    o.updateCrawlLogs(list_story[current_story-1].story_id);
                                    $('#list_story').append('<h3 class="kt-font-primary kt-font-bold"> ======= > Chuyển sang lấy dữ liệu truyện '+list_story[current_story].story_title+' <======= </h3>');
                                    $('.current-page').html(list_story[current_story].story_title+' - Số chương '+ list_chapter.length);
                                    
                                }
                                if(current_story == list_story.length && current_story.length != 0) {
                                    o.updateCrawlLogs(list_story[current_story-1].story_id);
                                    console.log('Đã hoàn thành');
                                    setTimeout(function () {
                                        $('.current_page').text('');
                                        $('.result-message').html('<h4 class="kt-font-primary kt-font-bold"> ======= > Đã hoàn thành quá trình lấy dữ liệu <======= </h4>');
                                        $('#list_story').append('<h3 class="kt-font-primary kt-font-bold"> ======= > Đã hoàn thành quá trình lấy dữ liệu <======= </h3>');
                                        $('#btn-crawl-story').prop('disabled', false);
                                        $('#btn-crawl-story').removeClass('disabled');
                                    }, 3000);
                                }
                            }
                            
                        },
                        error: function(errors) {
                            console.log(errors);
                            $('#list_story').append('<div class="kt-section__desc"><span class="kt-font-danger kt-font-italic">'+url+'</span> : '+errors.statusText+' </div>').slideDown("slow");
                            if(current_chapter < current_chapter.length) {
                                console.log('Thêm chương tiếp theo');
                                
                                current_story = current_story + 1;
                                addChapter(id, list_chapter[current_chapter]);
                                
                            }
                            if(current_chapter == list_chapter.length && list_chapter.length != 0) {
                                if(current_story < list_story.length) {
                                    console.log('Chuyển sang story tiếp theo');
                                    current_story = current_story + 1;
                                    var story_id = list_story[current_story].story_id;
                                    list_chapter = list_story[current_story].list_chapter;
                                    current_chapter = 0;
                                    console.log(current_chapter);
                                    console.log(list_chapter);
                                    o.addChapter(story_id,list_chapter[0])
                                    $('.current-page').html(list_story[current_story].story_title);
                                    
                                }
                                if(current_story == list_story.length && current_story.length != 0) {
                                    console.log('Đã hoàn thành');
                                    setTimeout(function () {
                                        $('.current_page').text('');
                                        $('.result-message').html('<h4 class="kt-font-primary kt-font-bold"> ======= > Đã hoàn thành quá trình lấy dữ liệu <======= </h4>');
                                        $('#list_story').append('<h3 class="kt-font-primary kt-font-bold"> ======= > Đã hoàn thành quá trình lấy dữ liệu <======= </h3>');
                                        $('#btn-crawl-story').prop('disabled', false);
                                        $('#btn-crawl-story').removeClass('disabled');
                                    }, 8000);
                                }
                            }
                            
                        }
                    });
                }
            },
        });
        jQuery(document).ready((function() {
            o.init();
            
        }))
    }
});
