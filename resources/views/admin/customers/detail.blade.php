@extends('admin.index')
@section('page-header', 'User')
@section('page-sub_header', 'Chi tiết User')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Tài Khoản Khách Hàng </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Xem Chi Tiết Khách Hàng
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_user_edit_form">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>ID:</label>
                                    <input type="text" class="form-control" value="{{ $userInfo->id }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control" value="{{ $userInfo->email }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Username:</label>
                                    <input type="text" class="form-control" placeholder="Nhập username" name="username"
                                        value="{{ $userInfo->username }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Họ và tên:</label>
                                    <input type="text" class="form-control" placeholder="Nhập họ và tên" name="full_name"
                                        value="{{ $userInfo->full_name }}" disabled>
                                </div>
                                <div class="form-group">
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_edit_avatar">
                                        <label>Avatar:</label>
                                        <div class="kt-avatar__holder"
                                            style="background-image: url('{{ $userInfo->avatar }}')"></div>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                            data-original-title="Cancel avatar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status:</label>
                                    <input type="text" class="form-control" placeholder="Nhập họ và tên" name="full_name"
                                        value="{{ ucfirst($userInfo->status) }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Phân quyền:</label>
                                    <div class="kt-checkbox-list">
                                        <?php $current_permisstion = explode(',', $userInfo->group_id); ?>
                                        @forelse($listGroup as $group)
                                            @if (in_array($group->id, $current_permisstion))
                                                <p><label>{{ $group->group_name }}</label></p>
                                            @endif
                                        @empty
                                            Không có quyền nào trên hệ thống
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Ngày Kích Hoạt:</label>
                                    <input type="text" class="form-control"
                                        value="{{ isset($userInfo->email_verified_at) ? date('d-m-Y H:i:s', strtotime($userInfo->email_verified_at)) : '' }}"
                                        disabled>
                                </div>
                                <div class="form-group">
                                    <label>Lần Truy Cập Cuối:</label>
                                    <input type="text" class="form-control"
                                        value="{{ isset($userInfo->last_visits) ? date('d-m-Y H:i:s', strtotime($userInfo->last_visits)) : '' }}"
                                        disabled>
                                </div>
                                <div class="form-group">
                                    <label>Created At:</label>
                                    <input type="text" class="form-control"
                                        value="{{ isset($userInfo->created_at) ? date('d-m-Y H:i:s', strtotime($userInfo->created_at)) : '' }}"
                                        disabled>
                                </div>
                                <div class="form-group">
                                    <label>Updated At:</label>
                                    <input type="text" class="form-control"
                                        value="{{ isset($userInfo->updated_at) ? date('d-m-Y H:i:s', strtotime($userInfo->updated_at)) : '' }}"
                                        disabled>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <a href="user" class="btn btn-secondary">Quay lại</a>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@endsection
@section('script')
<!--end::Page Vendors -->
<script src="assets/js/pages/custom/user/edit-user.js" type="text/javascript"></script>
<script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>

@endsection
