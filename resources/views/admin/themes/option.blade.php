@extends('admin.index')
@section('page-header', 'Giao Diện')
@section('page-sub_header', 'Tùy biến')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Tùy biến </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">

                        <form class="kt-form" id="kt_edit_form">
                            <div class="kt-portlet__body">
                                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-bold nav-tabs-line-primary" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#header" role="tab" aria-selected="false">Header</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#footer" role="tab" aria-selected="false">Footer</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    {{csrf_field()}}
                                    <div class="tab-pane active" id="header" role="tabpanel">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Copyright:</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" name="theme_option::header::copyright" value="{{isset($option['theme_option::header::copyright']) ? $option['theme_option::header::copyright'] : ''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Theme color:</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" name="theme_option::header::theme_color">
                                                    <option value="red" {{ isset($option['theme_option::header::theme_color']) && $option['theme_option::header::theme_color'] === 'red' ? 'selected' : ''}}>Red</option>
                                                    <option value="green" {{ isset($option['theme_option::header::theme_color']) && $option['theme_option::header::theme_color'] === 'green' ? 'selected' : ''}}>Green</option>
                                                    <option value="blue" {{ isset($option['theme_option::header::theme_color']) && $option['theme_option::header::theme_color'] === 'blue' ? 'selected' : ''}}>Blue</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Banner</label>
                                            <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="theme_option::header::banner">
                                                <label></label>
                                                <div class="kt-avatar__holder" style="background-image: url('{{isset($option['theme_option::header::banner']) ? $option['theme_option::header::banner'] : ''}}')"></div>
                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change banner">
                                                    <i class="fa fa-pen"></i>
                                                    <input type="file" name="theme_option::header::banner" accept="image/*">
                                                </label>
                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel banner">
                                                    <i class="fa fa-times"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="footer" role="tabpanel">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Copyright:</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" name="theme_option::footer::copyright" value="{{isset($option['theme_option::footer::copyright']) ? $option['theme_option::footer::copyright'] : ''}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Theme color:</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" name="theme_option::footer::theme_color">
                                                    <option value="red" {{ isset($option['theme_option::footer::theme_color']) && $option['theme_option::footer::theme_color'] === 'red' ? 'selected' : ''}}>Red</option>
                                                    <option value="green" {{ isset($option['theme_option::footer::theme_color']) && $option['theme_option::footer::theme_color'] === 'green' ? 'selected' : ''}}>Green</option>
                                                    <option value="blue" {{ isset($option['theme_option::footer::theme_color']) && $option['theme_option::footer::theme_color'] === 'blue' ? 'selected' : ''}}>Blue</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Banner</label>
                                            <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="theme_option::footer::banner">
                                                <label></label>
                                                <div class="kt-avatar__holder" style="background-image: url('{{isset($option['theme_option::footer::banner']) ? $option['theme_option::footer::banner'] : ''}}')"></div>
                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change banner">
                                                    <i class="fa fa-pen"></i>
                                                    <input type="file" name="theme_option::footer::banner" accept="image/*">
                                                </label>
                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel banner">
                                                    <i class="fa fa-times"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_edit" class="btn btn-primary">Cập Nhập</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="admin/js/pages/theme/theme-option.js?v1" type="text/javascript"></script>
@endsection