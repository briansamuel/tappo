<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link href="{{ url('') }}/css/profile_card.css" rel="stylesheet">
    <link rel="preload" as="script" type="text/css" href="{{ url('') }}/js/profile_card.js" />
</head>

<body>

    <div class="wrapper">
        <div class="profile-card js-profile-card">

            @include('frontsite.profiles.elements.profile_info')

            @include('frontsite.profiles.elements.list_social')

           
        </div>

    </div>

    @include('frontsite.profiles.elements.svg_social')

</body>
