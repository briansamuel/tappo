<div class="profile-card__img">
    <img src="{{ $user->avatar }}" alt="profile card">
</div>

<div class="profile-card__cnt js-profile-cnt">
    <div class="profile-card__name">{{ $user->full_name }}</div>
    <div class="profile-card__txt">{{ $user->description }}</div>
    <div class="profile-card-loc">
        <span class="profile-card-loc__icon">
            <svg class="icon">
                <use xlink:href="#icon-location"></use>
            </svg>
        </span>

        <span class="profile-card-loc__txt">
            {{ $user->address }}
        </span>
    </div>
</div>
