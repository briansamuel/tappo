@php
$list_social = json_decode($user->list_social);
$css = '';
$countBg = count($background);
if ($countBg > 1) {
    $arg_color = [];
    foreach ($background as $key => $color) {
        $percent = (100 / $countBg) * $key;
        if ($key == $countBg - 1) {
            $percent = '100';
        }
        $arg_color[] = $color . ' ' . $percent . '%';
    }
    $color = implode(', ', $arg_color);
    $css = 'background-image: linear-gradient(135deg, ' . $color . ');';
} else {
    $css = 'background: ' . $background[0] . ';';
}
@endphp
<div class="profile-card-social">
    @foreach ($list_social as $item)
        @php
            $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
            $url = $item->content;
           
            if ($item->icon == 'facebook') {
                $url = 'fb://facewebmodal/f?href=https://m.facebook.com/' . $item->content;
                if($iPhone){
                    $url = 'fb://profile/'. $item->content;
                   
                }
            } elseif ($item->icon == 'messenger') {
                $url = 'http://m.me/' . $item->content;
            } elseif ($item->icon == 'phone') {
                $url = 'tel:' . $item->content;
            } elseif ($item->icon == 'sms') {
                $url = 'sms:' . $item->content;
            } elseif ($item->icon == 'email') {
                $url = 'mailto:' . $item->content;
            } elseif ($item->icon == 'tiktok') {
                $url = 'https://www.tiktok.com/@' . $item->content;
            } elseif ($item->icon == 'instagram') {
                $url = 'instagram://user?username=' . $item->content;
                if($Android){
                    $url = 'http://www.instagram.com/'. $item->content;
                   
                }
            } elseif ($item->icon == 'twitter') {
                $url = 'twitter://user?screen_name=' . $item->content;
            } elseif ($item->icon == 'pinterest') {
                $url = 'pinterest://user/' . $item->content;
            } elseif ($item->icon == 'youtube') {
                $url = 'vnd.youtube://' . $item->content;
            }
        @endphp
        <a href="{{ $url }}" class="profile-card_social " target="_blank">
            <img src="{{ url('icons_img/social1/' . $item->icon) }}.png" />
            <span class="profile-card_text-social">{{ $item->label }}</span>
        </a>
    @endforeach


</div>
<style>
    .wrapper {
        @php
            echo $css;
        @endphp
    }

</style>
