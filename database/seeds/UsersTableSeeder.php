<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([
                'name' => Str::slug($faker->name, '_'),
                'email' => $faker->unique()->email,
                'password' => bcrypt('admin@123'),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
