<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CustomerssTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;
        $list_social = [
            array('label' => 'Facebook', 'content' => 'https://www.facebook.com/zenvux/', 'type' => 'url', 'icon' => 'facebook'),
            array('label' => 'Instagram', 'content' => 'https://www.instagram.com/wu.ptg/', 'type' => 'url', 'icon' => 'instagram'),
            array('label' => 'SMS', 'content' => '0869222907', 'type' => 'sms', 'icon' => 'sms'),
            array('label' => 'Website', 'content' => 'https://wdioconcept.com/anh-cua-wdio/', 'type' => 'url', 'icon' => 'website'),
            array('label' => 'Email', 'content' => 'dinhvanvu94@gmail.com', 'type' => 'email', 'icon' => 'email'),
            array('label' => 'Phone', 'content' => '0869222907', 'type' => 'phone', 'icon' => 'phone'),
        ];

        for ($i = 0; $i < $limit; $i++) {

            DB::table('customers')->insert([
                'username' => Str::slug($faker->name, '_'),
                'email' => $faker->unique()->email,
                'full_name' => $faker->name,
                'description' => 'Developer, Photographer Freelancer',
                'avatar' => $faker->imageUrl(200, 200, null),
                'password' => bcrypt('admin@123'),
                'address' => $faker->city(),
                'qr_code' => $faker->ean13,
                'list_social' => json_encode($list_social),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
